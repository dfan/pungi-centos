This repository holds the config files that define
the CentOS 9 Stream compose and RHEL 9 compose.

The repository is organized into several directories:

- ``shared`` - Pungi configuration files defining compose options
  which are shared between all Fedora ELN, CentOS Stream and RHEL.
  There is one Pungi configuration file for each Pungi phase and
  also ``general.conf``, ``multilib.conf`` and
  ``additional_and_filter_packages.conf`` defining compose options
  which are not specific for any particular phase.
- ``fedora`` - Pungi configuration files which extend the ``shared``
  compose options (and in some cases overrides them) for Fedora ELN
  compose.
- ``centos`` - Pungi configuration files which extend the ``shared``
  compose options (and in some cases overrides them) for CentOS
  compose.
- ``rhel`` - Pungi configuration files which extend the ``shared``
  compose options (and in some cases overrides them) for RHEL
  compose.

In the main directory, there is one file for each compose which can
be generated from thie configuration files repository. Each config
file imports the ``shared`` configuration files and also the ``fedora``,
``centos`` or ``rhel`` specific overrides.

The config files should use spaces and not tabs for whitespace
formatting. Note that the pungi config. files use binary package
names, not source package names (Eg. python3-X), which means
you'll need to list each required sub-package.
Also we've started lexically ordering the sections, so that it's
easier to see which packages have been added, as a section gets big.

#testline for pipeline